const mongoose = require('mongoose');

// app.js dotor db require hiisen, daraa ni router require hiisen, router dotor ctrl, ctrl dotor db require hiigdsen bga.
// tiim uchir end Game model - g ashiglaj bolno.
const Game = mongoose.model('Game');

const runGeoQuery = function (req, res) {
    const lat = parseFloat(req.query.lat);
    const lng = parseFloat(req.query.lng);

    console.log('Geo search long, lat', lng, lat);

    const query = {
        "publisher.location": {
            $near: {
                $geometry: {
                    type: "Point",
                    coordinates: [lng, lat]
                },
                $maxDistance: 1000,
                $minDistance: 0
            }
        }
    };
    Game.find(query).exec(function (err, games) {
        console.log('Found games with Geo search', games);
        res.status(200).json(games);
    });
}

module.exports.gamesGetAll = function (req, res) {
    console.log("GetALl games");

    if (req.query && req.query.lat && req.query.lng) {
        runGeoQuery(req, res);
        return;
    }

    // use pagination
    let count = 5;
    if (req.query && req.query.count) {
        count = parseInt(req.query.count);
    }
    let offset = 0;
    if (req.query && req.query.offset) {
        offset = parseInt(req.query.offset);
    }

    // const db = dbConnection.get();
    // const collection = db.collection('games');
    // // const games = collection.find();  // this is blocking code
    // collection.find().skip(offset).limit(count).toArray(function (err, docs) {
    //     res.status(200).send(docs);
    // });

    Game.find().skip(offset).limit(count).exec(function (err, games) {
        res.status(200).json(games);
    });

}

module.exports.gamesGetOne = function (req, res) {
    console.log("GetOne games");

    // path param avah
    const gameId = req.params.gameId;
    console.log('gameId', gameId);

    Game.findById(gameId).exec(function (err, game) {
        res.status(200).json(game);
    });
}


module.exports.gamesAddOne = function (req, res) {
    console.log("GetOne games");

    const newGame = {
        title: req.body.title,
        year: parseInt(req.body.year),
        price: parseFloat(req.body.price),
        minPlayers: parseInt(req.body.minPlayers),
        maxPlayers: parseInt(req.body.maxPlayers),
        minAge: parseInt(req.body.minAge),
        designers: [],
        publisher: {}
    };
    if (req.body.rate) {
        newGame.rate = parseInt(req.body.rate);
    }

    console.log('newGame', newGame);

    Game.create(newGame, function (err, game) {
        const response = {
            status: 201,
            message: game
        };
        if (err) {
            console.log("Error creating game", err);
            response.status = 500;
            response.message = "Internal error";
        }
        console.log('Game saved', game);
        res.status(response.status).json(response.message);
    });
}

// create new partial update function!!!

const _updateGame = function (req, res, game) {
    console.log('_updateGame', game);
    game.title = req.body.title;
    game.year = parseInt(req.body.year);
    game.price = parseFloat(req.body.price);
    game.minPlayers = parseInt(req.body.minPlayers);
    game.maxPlayers = parseInt(req.body.maxPlayers);
    game.minAge = parseInt(req.body.minAge);


    // partial update
    if (req.body.rate) {
        game.rate = parseInt(req.body.rate);
    }

    game.save(function (err, game) {
        const response = {
            status: 201,
            message: game
        };
        if (err) {
            console.log("Error creating game", err);
            response.status = 500;
            response.message = "Internal error";
        }
        console.log('Game saved', game);
        res.status(response.status).json(response.message);
    });
}

module.exports.gamesUpdateOne = function (req, res) {
    console.log("GetOne games");

    const gameId = req.params.gameId;
    Game.findById(gameId).exec(function (err, game) {
        const response = {
            status: 200
        };

        if (err) {
            response.status = 500;
            response.message = 'Internal error';
        } else if (!game) {
            response.status = 404;
            response.message = 'Game not found';
        }
        if (response.status === 200) {
            // found game
            _updateGame(req, res, game);
        } else {
            res.status(response.status).json(response.message);
        }

    });

}
meanGames.factory("GameService", GameService);

const _url = "/api/games";

function GameService($http) {
    return {
        getAllGames: getAll,
        getGame: getOne
    }

    function getAll() {
        return $http.get(_url)
            .then(complete)
            .catch(failed);
    }

    function getOne(id) {
        return $http.get(_url + "/" + id)
            .then(complete)
            .catch(failed);
    }

    function complete(res) {
        return res.data;
    }

    function failed(error) {
        return error;
    }
}